package @packageName@.di

import android.app.Application
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

@Component(modules = arrayOf(AndroidInjectionModule::class, AndroidSupportInjectionModule::class))
interface ApplicationComponent {
    fun inject(application: Application)
}