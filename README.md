# Android-template-project 

The same way `maven` uses `archetypes` to create sample projects, we use a gradle archetype plugin to create an Android template project with the following libraries: **Dagger 2, RXJava + RXAndroid, Retrofit, Espresso, Junit, mockito and hamcrest for testing.**

We use the following plugin to generate the android template project: https://github.com/orctom/gradle-archetype-plugin

### HOW TO:
* Edit the file `gradle.properties` and select your desired `group`, `name` and `version`. The `group` will be our `package` and the `name` will be the App's name.
* Run the command: `./gradlew cleanArch generate`.
* The generated android project will be located in: `android-template-project/generated/`
* The file `android-template-project/src/main/resources/.nontemplates` contains the resources and folders that won't be scanned, just copied as they are.
* Folders and files under 'android-template-project/src/main/resources/templates' will be scanned and templated accordingly.


